// TODO : 

// trouver une solution pour les grosses duplications de code
// refaire le bouton reset pour éviter de réactualiser la page
// petit bug occasionnel d'affichage sur la bordure des cercles dans les blocs
// trouver une solution plus élégante pour comparer les couleurs 

// ---------------------------------------------------------------------------------

// Fonction de création de nombres aléatoire
function randomNumber(min, max){
    if (typeof min !== "number" || isNaN(min)){
        return
    } else {
        let random = Math.floor(Math.random() * (max-min)) + min
        return random
    }
}

// Création de la combinaison aléatoire de l'ordinateur en fonction de la difficulté choisie
function random (){
    let tableauOrdinateur = [];
    for(let index =0; index<4; index++) {
        tableauOrdinateur.push(randomNumber(1, 5))
    } 
    return tableauOrdinateur
}

function randomDifficile (){
    let tableauOrdinateur = [];
    for(let index =0; index<4; index++) {
        tableauOrdinateur.push(randomNumber(1, 6))
    } 
    return tableauOrdinateur
}

function randomExtreme (){
    let tableauOrdinateur = [];
    for(let index =0; index<4; index++) {
        tableauOrdinateur.push(randomNumber(1, 7))
    } 
    return tableauOrdinateur
}

// Comportement lorsque le joueur gagne
function effetClignotant (){
    // Effet visuel clignotant pour le bouton bleu
    setInterval(function() {
        let turnColorBlue = document.getElementById("blue");
        let colorBlue = ["rgb(135, 176, 234)", "rgb(66, 79, 145)"];
        turnColorBlue.style.backgroundColor = colorBlue[b];
        b = (b + 1) % colorBlue.length;

    }, 250);

    // Effet visuel clignotant pour le bouton jaune
    setInterval(function() {
        let turnColorYellow = document.getElementById("yellow");
        let colorYellow = ["rgb(255, 246, 162)", "rgb(123, 108, 41)"];
        turnColorYellow.style.backgroundColor = colorYellow[y];
        y = (y + 1) % colorYellow.length;           
    }, 250);

    // Effet visuel clignotant pour le bouton vert
    setInterval(function() {
        let turnColorGreen = document.getElementById("green");
        let colorGreen = ["rgb(154, 210, 123)", "rgb(59, 112, 62)"];
        turnColorGreen.style.backgroundColor = colorGreen[g];
        g = (g + 1) % colorGreen.length;               
    }, 250);

    // Effet visuel clignotant pour le bouton jaune
    setInterval(function() { 
        let turnColorRed = document.getElementById("red");
        let colorRed = ["#db6060", "rgb(122, 35, 35)"];
        turnColorRed.style.backgroundColor = colorRed[j];
        j = (j + 1) % colorRed.length;                
    }, 250);

    setInterval(function() { 
        let turnColorPink = document.getElementById("pink");
        let colorPink = ["rgb(227, 166, 212)", "rgb(136, 66, 125)"];
        turnColorPink.style.backgroundColor = colorPink[p];
        p = (p + 1) % colorPink.length;                
    }, 250);

    setInterval(function() { 
        let turnColorOrange = document.getElementById("orange");
        let colorOrange = ["rgb(223, 156, 107)", "rgb(132, 86, 37)"];
        turnColorOrange.style.backgroundColor = colorOrange[o];
        o = (o + 1) % colorOrange.length;                
    }, 250);
}

let redButton = document.querySelector("#red")
let yellowButton = document.querySelector("#yellow")
let greenButton = document.querySelector("#green")
let blueButton = document.querySelector("#blue")
let pinkButton = document.querySelector("#pink")
let orangeButton = document.querySelector("#orange")
let valideButton = document.querySelector('#valide')
let clearButton = document.querySelector('#clear')
let resetButton = document.querySelector('#reset')
let affichageAllumage = document.querySelector('.affichageAllumage')
let affichageOn = document.querySelector('.affichageOn')
let affichagelogo = document.querySelector(".affichagelogo")
let containerAideAllumage =document.querySelector(".containerAideAllumage")
let dificult = document.querySelector(".dificult")
let normal = document.querySelector("#normal")
let difficile = document.querySelector("#difficile")
let extreme = document.querySelector("#extreme")

dificult.style.display = "none"
pinkButton.style.display= "none"
orangeButton.style.display= "none"

// Comportement lorsque la difficulté est choisie
normal.addEventListener('click', function(){
    affichagelogo.textContent = "Mastermind"
    dificult.style.display = "none" 
    resultatOrdinateur = random()
    demmarrageJeux = true
})

difficile.addEventListener('click', function(){
    affichagelogo.textContent = "Mastermind"
    dificult.style.display = "none" 
    pinkButton.style.display= "block"
    resultatOrdinateur = randomDifficile()
    demmarrageJeux = true
})
extreme.addEventListener('click', function(){
    affichagelogo.textContent = "Mastermind"
    dificult.style.display = "none" 
    pinkButton.style.display= "block"
    orangeButton.style.display= "block"
    resultatOrdinateur = randomExtreme()
    demmarrageJeux = true
})

let demmarrageJeux = false

// Bouton On qui affiche les niveaux de difficulté
let affichageDifficulteBoutonOn = true
affichageAllumage.addEventListener('click', function(){
    affichageOn.style.color = "rgb(188, 253, 122)"
    containerAideAllumage.style.display = "none"
    affichagelogo.textContent = ""
    if (affichageDifficulteBoutonOn === true){
        dificult.style.display = "block"
        affichageDifficulteBoutonOn = false
    }
    clearInterval(aideAllumageInterval)
})

// Chaque bloc représente l'emplacement pour chaque essai
let bloc = document.querySelector(".bloc:nth-child(10)")

// Comportement du scroll lorsque les essais ne rentrent plus à l'écran 
bloc.scrollIntoView({
    behavior: "smooth"
})

// Les boutons représentent l'emplacement où seront affichés les couleurs pour chaque essais 
let selectionBouton1 = bloc.querySelector(".selectionBouton1")
let selectionBouton2 = bloc.querySelector(".selectionBouton2")
let selectionBouton3 = bloc.querySelector(".selectionBouton3")
let selectionBouton4 = bloc.querySelector(".selectionBouton4")

// Les pions représentent l'aide visuelles lorsqu'un joueur valide un essai
let info = document.querySelector(".info:nth-child(10)")
let pion1 = bloc.querySelector(".pion1")
let pion2 = bloc.querySelector(".pion2")
let pion3 = bloc.querySelector(".pion3")
let pion4 = bloc.querySelector(".pion4")


// Emplacement pour l'aide textuelle
let affichageIndice = document.querySelector(".indiceContent")
let affichageAide = document.querySelector(".affichageAide")

// Variables des couleurs clignotantes
let b = 0;
let y = 0;
let g = 0;
let j = 0;
let p = 0;
let o = 0;

// Nombres de cases minimum du jeux. Les cases représentent le nombre de couleurs disponibles à deviner
let nbrCase = 4

// Initialisation du nombre d'essai pour le début de partie
let nbrEssai = 0

// Tableau qui représente (en chiffre) la combinaison validée pour chaque essai
let resultatUtilisateur= []


// -----------------------------------------------------
// Gestion des évènements au clic pour chaque couleur
// -----------------------------------------------------

blueButton.addEventListener('click', function(){
    let blue = 1
    if (demmarrageJeux === true){
        if(resultatUtilisateur.length < nbrCase){
            resultatUtilisateur.push(blue)
    
            if (resultatUtilisateur[0] === 1) {
                selectionBouton1.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px;';
                selectionBouton1.style.boxShadow = "2px -3px 7px 3px rgba(32,53,164) inset";
                selectionBouton1.style.background = "rgb(102, 148, 212)"
            } 
            if (resultatUtilisateur[1] === 1) {
                selectionBouton2.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px;';
                selectionBouton2.style.boxShadow = "2px -3px 7px 3px rgba(32,53,164) inset";
                selectionBouton2.style.background = "rgb(102, 148, 212)"
            }
            if (resultatUtilisateur[2] === 1) {
                selectionBouton3.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px;';
                selectionBouton3.style.boxShadow = "2px -3px 7px 3px rgba(32,53,164) inset";
                selectionBouton3.style.background = "rgb(102, 148, 212)"
            }
            if (resultatUtilisateur[3] === 1) {
                selectionBouton4.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px;';
                selectionBouton4.style.boxShadow = "2px -3px 7px 3px rgba(32,53,164) inset";
                selectionBouton4.style.background = "rgb(102, 148, 212)"
            }
        }
        affichagelogo.textContent = "Mastermind"
    }
    affichageAide.style.display = "none"
    
})

yellowButton.addEventListener('click', function(){
    let yellow = 2
    if (demmarrageJeux === true){
        if(resultatUtilisateur.length < nbrCase){
            resultatUtilisateur.push(yellow)
    
            if (resultatUtilisateur[0] === 2) {
                selectionBouton1.style.background = 'rgb(255, 246, 162)'
                selectionBouton1.style.border = "solid hsl(160, 100%, 75%, 0.3) 1px"
                selectionBouton1.style.boxShadow = "2px -3px 7px 3px rgb(123, 108, 41) inset"
            } 
            if (resultatUtilisateur[1] === 2) {
                selectionBouton2.style.background = 'rgb(255, 246, 162)'
                selectionBouton2.style.border = "solid hsl(160, 100%, 75%, 0.3) 1px"
                selectionBouton2.style.boxShadow = "2px -3px 7px 3px rgb(123, 108, 41) inset"
            }
            if (resultatUtilisateur[2] === 2) {
                selectionBouton3.style.background = 'rgb(255, 246, 162)'
                selectionBouton3.style.border = "solid hsl(160, 100%, 75%, 0.3) 1px"
                selectionBouton3.style.boxShadow = "2px -3px 7px 3px rgb(123, 108, 41) inset"
            }
            if (resultatUtilisateur[3] === 2) {
                selectionBouton4.style.background = 'rgb(255, 246, 162)'
                selectionBouton4.style.border = "solid hsl(160, 100%, 75%, 0.3) 1px"
                selectionBouton4.style.boxShadow = "2px -3px 7px 3px rgb(123, 108, 41) inset"
            }
        }
        affichagelogo.textContent = "Mastermind"
    }
    affichageAide.style.display = "none"  
})

greenButton.addEventListener('click', function(){
    let green = 3
    if (demmarrageJeux === true){
        if(resultatUtilisateur.length < nbrCase){
            resultatUtilisateur.push(green)
    
            if (resultatUtilisateur[0] === 3) {
                selectionBouton1.style.background = 'rgb(154, 210, 123)'
                selectionBouton1.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px'
                selectionBouton1.style.boxShadow = '2px -3px 7px 3px rgb(59, 112, 62) inset'
    
            } 
            if (resultatUtilisateur[1] === 3) {
                selectionBouton2.style.background = 'rgb(154, 210, 123)'
                selectionBouton2.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px'
                selectionBouton2.style.boxShadow = '2px -3px 7px 3px rgb(59, 112, 62) inset'
    
            }
            if (resultatUtilisateur[2] === 3) {
                selectionBouton3.style.background = 'rgb(154, 210, 123)'
                selectionBouton3.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px'
                selectionBouton3.style.boxShadow = '2px -3px 7px 3px rgb(59, 112, 62) inset'
    
            }
            if (resultatUtilisateur[3] === 3) {
                selectionBouton4.style.background = 'rgb(154, 210, 123)'
                selectionBouton4.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px'
                selectionBouton4.style.boxShadow = '2px -3px 7px 3px rgb(59, 112, 62) inset'
            }
        }
        affichagelogo.textContent = "Mastermind"
    }
    affichageAide.style.display = "none"
})

redButton.addEventListener('click', function(){
    let red = 4
    if (demmarrageJeux === true){
        if(resultatUtilisateur.length < nbrCase){
            resultatUtilisateur.push(red)
    
            if (resultatUtilisateur[0] === 4) {
                selectionBouton1.style.background = '#db6060'
                selectionBouton1.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px'
                selectionBouton1.style.boxShadow = '2px -3px 7px 3px rgb(122, 35, 35) inset'
            } 
            if (resultatUtilisateur[1] === 4) {
                selectionBouton2.style.background = '#db6060'
                selectionBouton2.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px'
                selectionBouton2.style.boxShadow = '2px -3px 7px 3px rgb(122, 35, 35) inset'
            }
            if (resultatUtilisateur[2] === 4) {
                selectionBouton3.style.background = '#db6060'
                selectionBouton3.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px'
                selectionBouton3.style.boxShadow = '2px -3px 7px 3px rgb(122, 35, 35) inset'
            }
            if (resultatUtilisateur[3] === 4) {
                selectionBouton4.style.background = '#db6060'
                selectionBouton4.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px'
                selectionBouton4.style.boxShadow = '2px -3px 7px 3px rgb(122, 35, 35) inset'
            }
        }
        affichagelogo.textContent = "Mastermind"
    }
    affichageAide.style.display = "none"
})

pinkButton.addEventListener('click', function(){
    let pink = 5
    if (demmarrageJeux === true){
        if(resultatUtilisateur.length < nbrCase){
            resultatUtilisateur.push(pink)
    
            if (resultatUtilisateur[0] === 5) {
                selectionBouton1.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px;';
                selectionBouton1.style.boxShadow = "2px -3px 7px 3px rgb(136, 66, 125) inset";
                selectionBouton1.style.background = "rgb(227, 166, 212)"
            } 
            if (resultatUtilisateur[1] === 5) {
                selectionBouton2.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px;';
                selectionBouton2.style.boxShadow = "2px -3px 7px 3px rgb(136, 66, 125) inset";
                selectionBouton2.style.background = "rgb(227, 166, 212)"
            }
            if (resultatUtilisateur[2] === 5) {
                selectionBouton3.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px;';
                selectionBouton3.style.boxShadow = "2px -3px 7px 3px rgb(136, 66, 125) inset";
                selectionBouton3.style.background = "rgb(227, 166, 212)"
            }
            if (resultatUtilisateur[3] === 5) {
                selectionBouton4.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px;';
                selectionBouton4.style.boxShadow = "2px -3px 7px 3px rgb(136, 66, 125) inset";
                selectionBouton4.style.background = "rgb(227, 166, 212)"
            }
        }
        affichagelogo.textContent = "Mastermind"
    }
    affichageAide.style.display = "none"
})

orangeButton.addEventListener('click', function(){
    let orange = 6
    if (demmarrageJeux === true){
        if(resultatUtilisateur.length < nbrCase){
            resultatUtilisateur.push(orange)
    
            if (resultatUtilisateur[0] === 6) {
                selectionBouton1.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px;';
                selectionBouton1.style.boxShadow = "2px -3px 7px 3px rgb(132, 86, 37) inset";
                selectionBouton1.style.background = "rgb(223, 156, 107)"
            } 
            if (resultatUtilisateur[1] === 6) {
                selectionBouton2.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px;';
                selectionBouton2.style.boxShadow = "2px -3px 7px 3px rgb(132, 86, 37) inset";
                selectionBouton2.style.background = "rgb(223, 156, 107)"
            }
            if (resultatUtilisateur[2] === 6) {
                selectionBouton3.style.border = 'solid hsl(160, 100%, 75%, 0.3) 1px;';
                selectionBouton3.style.boxShadow = "2px -3px 7px 3px rgb(132, 86, 37) inset";
                selectionBouton3.style.background = "rgb(223, 156, 107)"
            }
            if (resultatUtilisateur[3] === 6) {
                selectionBouton4.style.border = 'solid rgba(255, 242, 128, 0.3) 2px;';
                selectionBouton4.style.boxShadow = "2px -3px 7px 3px rgb(132, 86, 37) inset";
                selectionBouton4.style.background = "rgb(223, 156, 107)"
            }
        }
        affichagelogo.textContent = "Mastermind"
    }
    affichageAide.style.display = "none"
})

// ----------------------------------------------------------------------------
// Gestion des évènements au clic pour les Boutons Reset, Clear, et Validation
// ----------------------------------------------------------------------------

// Supprime la derniere bille de la proposition en cours ainsi que son affichage
clearButton.addEventListener('click', function(){
    resultatUtilisateur = resultatUtilisateur.slice(0, -1)

    if (resultatUtilisateur.length === 3) {
        selectionBouton4.style.background = "#213558"
        selectionBouton4.style.boxShadow = "none"
        selectionBouton4.style.border ="none"
    }
    if (resultatUtilisateur.length === 2) {
        selectionBouton3.style.background = "#213558"
        selectionBouton3.style.boxShadow = "none"
        selectionBouton3.style.border ="none"
    }
    if (resultatUtilisateur.length === 1) {
        selectionBouton2.style.background = "#213558"
        selectionBouton2.style.boxShadow = "none"
        selectionBouton2.style.border ="none"
    }
    if (resultatUtilisateur.length === 0) {
        selectionBouton1.style.background = "#213558"
        selectionBouton1.style.boxShadow = "none"
        selectionBouton1.style.border ="none"
    }
    affichageAide.style.display = "none"
})

// Réactualisation de la page
resetButton.addEventListener('click', function(){
    document.location.reload();
    affichageAide.style.display = "none"
})

// Lorsque l'on click sur le boutton valider
valideButton.addEventListener('click', function(){
    if (resultatUtilisateur.length === nbrCase){

        let tabPion = []
        if (demmarrageJeux===true){
            affichageAide.style.display = "block"
        }
    
        nbrEssai++
        
        let goodPlace = 0
        let misPlaced = 0

        for (index = 0; index<4; index++){
            if (resultatOrdinateur[index] == resultatUtilisateur[index]){
                goodPlace++
            }
        }

        let absent = 0
        let compteurOrdi = 0
        let compteurUtilisateur = 0
        
        let compteurUser1 = 0
        let compteurUser2 = 0
        let compteurUser3 = 0
        let compteurUser4 = 0
        let compteurUser5 = 0
        let compteurUser6 = 0
        
        
        let compteurOrdi1 = 0
        let compteurOrdi2 = 0
        let compteurOrdi3 = 0
        let compteurOrdi4 = 0
        let compteurOrdi5 = 0
        let compteurOrdi6 = 0
        
        
        // On passe en revue tout les index du resultat de l'ordinateur pour les comparer avec l'index de la proposition pour savoir si la bille est présente
        for (index = 0 ; index < 4 ; index++) {
            colorPresentInUser = resultatOrdinateur.indexOf(resultatUtilisateur[index])
            colorPresentInPC = resultatUtilisateur.indexOf(resultatOrdinateur[index])
            if (colorPresentInUser != -1 && resultatOrdinateur[index] !== resultatUtilisateur[index]){
        
                misPlaced++
                if (resultatUtilisateur[index] === 6){
                    compteurUtilisateur++
                    compteurUser6++
                } 
                if (resultatUtilisateur[index] === 5){
                    compteurUtilisateur++
                    compteurUser5++
                } 
                if (resultatUtilisateur[index] === 4){
                    compteurUtilisateur++
                    compteurUser4++
                }
                if (resultatUtilisateur[index] === 3){
                    compteurUtilisateur++
                    compteurUser3++
                }
                if (resultatUtilisateur[index] === 2){
                    compteurUtilisateur++
                    compteurUser2++
                }
                if (resultatUtilisateur[index] === 1){
                    compteurUtilisateur++
                    compteurUser1++
                }
            }
            if (colorPresentInPC != -1 && resultatOrdinateur[index] !== resultatUtilisateur[index]){
                if (resultatOrdinateur[index] === 6){
                    compteurOrdi++
                    compteurOrdi6++
                }
                if (resultatOrdinateur[index] === 5){
                    compteurOrdi++
                    compteurOrdi5++
                }
                if (resultatOrdinateur[index] === 4){
                    compteurOrdi++
                    compteurOrdi4++
        
                }
                if (resultatOrdinateur[index] === 3){
                    compteurOrdi++
                    compteurOrdi3++
        
                }
                if (resultatOrdinateur[index] === 2){
                    compteurOrdi++
                    compteurOrdi2++
                }
                if (resultatOrdinateur[index] === 1){
                    compteurOrdi++
                    compteurOrdi1++
                }
            }
        }
        
        let compttotal1 = compteurUser1 - compteurOrdi1
        if (compttotal1<0){
            compttotal1 =0
        }
        
        let compttotal2 = compteurUser2 - compteurOrdi2
        if (compttotal2<0){
            compttotal2 =0
        }
        
        let compttotal3 = compteurUser3 - compteurOrdi3
        if (compttotal3<0){
            compttotal3 =0
        }
        
        let compttotal4 = compteurUser4 - compteurOrdi4
        if (compttotal4<0){
            compttotal4 =0
        }
        let compttotal5 = compteurUser5 - compteurOrdi5
        if (compttotal5<0){
            compttotal5 =0
        }
        
        let compttotal6 = compteurUser6 - compteurOrdi6
        if (compttotal6<0){
            compttotal6 =0
        }
        
        absent = compttotal1 + compttotal2 +compttotal3 + compttotal4 + compttotal5 + compttotal6
        
        misPlaced = misPlaced - absent
        absent = resultatUtilisateur.length - misPlaced - goodPlace
        
        let tableau = []
        
        for (i=0; i<goodPlace ; i++){
            if (goodPlace > 0){
                tableau.push("goodPlace")
            }
        }
        for (j=0; j<misPlaced ; j++){
            if (misPlaced > 0){
                tableau.push("misPlaced")
            }
        }
        
       
        // Affichage lorsque le joueur gagne
        if (goodPlace === 4) {
            affichageIndice.textContent = "YOU WIN ! ☺"
            affichageIndice.style.fontSize = 2 + "rem"
            affichageIndice.style.fontFamily = "Roboto"
            affichagelogo.textContent = ""
            effetClignotant()
        } else {
            if (demmarrageJeux===true){
                // Affichage du message dans la bulle d'information ♦ bonnes places, ♦ mauvaises places, ♦ innexistantes
                affichageIndice.textContent = "Vous avez " + goodPlace + " couleur(s) à la bonne(s) place(s), " + "et " + misPlaced + " couleurs aux mauvais emplacement(s)" 
                if (absent>0){
                    affichageIndice.textContent = affichageIndice.textContent + ", et " + absent + " mauvaise(s) couleur(s)"
                }
            }
        }

        // Affichage lorsque le joueur perd
        if (nbrEssai === 10 && goodPlace !==4){
            affichagelogo.textContent = "Perdu !"
            affichageIndice.textContent = ""

        }

        // Pour chaque essai on passe au bloc superieur
        let row = (10 - nbrEssai )
        bloc = document.querySelector(".bloc:nth-child("+ row +")")
        bloc.scrollIntoView({
            behavior: "smooth" 
        })
        selectionBouton1 = bloc.querySelector(".selectionBouton1")
        selectionBouton2 = bloc.querySelector(".selectionBouton2")
        selectionBouton3 = bloc.querySelector(".selectionBouton3")
        selectionBouton4 = bloc.querySelector(".selectionBouton4")
    
        // Pour chaque essai on passe au bloc de pions superieur 
        let help = ((10 - nbrEssai) + 1)
        bloc = document.querySelector(".bloc:nth-child("+ help +")")
        pion1 = bloc.querySelector(".pion1")
        pion2 = bloc.querySelector(".pion2")
        pion3 = bloc.querySelector(".pion3")
        pion4 = bloc.querySelector(".pion4") 
    
        // Affichage d'un pion de couleur vert pour les propostitions à la bonnes places

        for (index = 0 ; index < tableau.length ; index++) {
            if (tableau[0] === "goodPlace") {
                pion1.style.background = 'green'
            } 
            if (tableau[1] === "goodPlace") {
                pion2.style.background = 'green'
            }
            if (tableau[2] === "goodPlace") {
                pion3.style.background = 'green'
            }
            if (tableau[3] === "goodPlace") {
                pion4.style.background = 'green'
            }

            if (tableau[0] === "misPlaced") {
                pion1.style.background = "white"
            } 
            if (tableau[1] === "misPlaced") {
                pion2.style.background = 'white'
            }
            if (tableau[2] === "misPlaced") {
                pion3.style.background = 'white'
            }
            if (tableau[3] === "misPlaced") {
                pion4.style.background = 'white'
            }
        }
        
        affichagelogo.textContent = ""
        
        // Vide le tableau de proposition pour la prochaine proposition de l'utilisateur
        resultatUtilisateur = []
    }
})


// aide pour l'allumage du Mastermin via le bouton On
let aideAllumage = document.querySelector(".aideAllumage")
  
let aideAllumageInterval = setInterval(function() {
    aideAllumage.style.display = "block"    
    affichagelogo.textContent = ""

}, 4500);




